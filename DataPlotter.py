import pandas as pd
import matplotlib.pyplot as plt
import os

path = os.path.dirname(os.path.realpath("__file__"))

df = pd.read_csv(path+"\\teste.csv")

plt.plot(df)
plt.xlabel('num leituras')
plt.ylabel('Força (Kgf)')
plt.show()

