// INCLUSÃO DE BIBLIOTECAS
#include <HX711.h>
#include <SD.h>

// DEFINIÇÕES DE PINOS
#define pinDT  2
#define pinSCK  3

#define escala 9600.0f

// INSTANCIANDO OBJETOS
HX711 strain;

// DECLARAÇÃO DE VARIÁVEIS  
float medida=0;
int chipSelect = 4;
File Data;

void setup() {
  Serial.begin(9600);

  strain.begin(pinDT, pinSCK); // CONFIGURANDO OS PINOS DA BALANÇA
  strain.set_scale(escala); // ENVIANDO O VALOR DA ESCALA CALIBRADO

  delay(2000);
  strain.tare(); // ZERANDO A BALANÇA PARA DESCONSIDERAR A MASSA DA ESTRUTURA
  SD.begin(chipSelect);
}

void loop() {
    
  strain.power_up(); // LIGANDO O SENSOR
  
  medida = strain.get_units(5); // SALVANDO NA VARIAVEL O VALOR DA MÉDIA DE 5 MEDIDAS

  Serial.println(medida,3);

  Data = SD.open("Data.txt", FILE_WRITE);
  if (Data){
    Data.println(medida);
  }

  delay(100);
  }
    
